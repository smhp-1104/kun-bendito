﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyerCollider : MonoBehaviour
{
    // Start is called before the first frame update

    public bool limithit;


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.CompareTag("ColliderLimit")) 
        {
            limithit = true;
        }

        if (other.gameObject && limithit == false)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("ColliderLimit"))
        {
            limithit = false;
        }
    }
}
