﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inmovilizar : MonoBehaviour
{
    public float timer;
    public float timer2;

    public PlayerMovement movimiento;
    // Start is called before the first frame update
    void Start()
    {
        movimiento.enabled = false;
        
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer >= 4f)
        {
            movimiento.enabled = false;
        }

        timer2 += Time.deltaTime;
        if (timer2 >= 12f)
        {
            timer = -100000;
            movimiento.enabled = true;

        }
    }
}
