﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Health_Enemy1 : MonoBehaviour
{
    // Start is called before the first frame update
    public Estadisticas_R player;

    public HealthBar healthbar;
    public ParticleSystem deathParticles;
    public Rigidbody rb;
    public int dmgfromPlayer;
    public int dmgfromBullet;
    public float vspeed;
    public bool tutorialrespawn;
    public float timer;
    public SkinnedMeshRenderer nupMesh;
    public Material matNormal;
    public Material matHitted;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        healthbar.damage = dmgfromPlayer;
        player = GameObject.FindGameObjectWithTag("Jugador").GetComponent<Estadisticas_R>();

        nupMesh.material = matNormal;
    }

    // Update is called once per frame
    void Update()
    {

        if (healthbar.health<=0)
        {
            Destroy(gameObject);
            player.enemigos += 1;
        }
        timer += Time.deltaTime;

        if (timer >= 0.3)
        {
            nupMesh.material = matNormal;
        }
    }



    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("ArmaPlayer"))
        {
            if (healthbar && timer >= 0.5)
            {
                nupMesh.material = matHitted;
                timer = 0;
                healthbar.damage = dmgfromPlayer;
                
                healthbar.OnTakeDamage();             

            }

        }

        if (collision.gameObject.CompareTag("Bullet"))
        {
            if (healthbar && timer >= 0.5)
            {
                nupMesh.material = matHitted;
                timer = 0;
                healthbar.damage = dmgfromBullet;

                healthbar.OnTakeDamage();
            }

        }


    }

}
