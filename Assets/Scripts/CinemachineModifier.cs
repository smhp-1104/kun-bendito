﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CinemachineModifier : MonoBehaviour
{
    // Start is called before the first frame update

    public CinemachineBrain cbrain;
    public Transform Target, Player;
    public Transform Obstruction;
    float zoomSpeed = 2f;
    public AudioSource music;
    public CinemachineFreeLook camnormal;
    public CinemachineFreeLook camapuntada;


    void Start()
    {
        camnormal.m_YAxis.m_MaxSpeed = 1.75f;
        camnormal.m_XAxis.m_MaxSpeed = 175;
        camapuntada.m_YAxis.m_MaxSpeed = 1.75f;
        camapuntada.m_XAxis.m_MaxSpeed = 175;
    }


    void Update()
    {
        Disparador_S disparador = GameObject.FindGameObjectWithTag("ArmaDisparo").GetComponent<Disparador_S>();

        if (Input.GetKeyUp(KeyCode.P))
        {
            cbrain.enabled = !cbrain.enabled;
        }

        if (disparador.disparando == true)
        {
            //music.pitch = 0.8f;          Para lentear la musica
        }
        else
        {
            //music.pitch = 1;             Para acelerar la musica
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            camnormal.m_YAxis.m_MaxSpeed = 1;
            camnormal.m_XAxis.m_MaxSpeed = 100;
            camapuntada.m_YAxis.m_MaxSpeed = 1;
            camapuntada.m_XAxis.m_MaxSpeed = 100;
            
        }

        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            camnormal.m_YAxis.m_MaxSpeed = 1.75f;
            camnormal.m_XAxis.m_MaxSpeed = 175;
            camapuntada.m_YAxis.m_MaxSpeed = 1.75f;
            camapuntada.m_XAxis.m_MaxSpeed = 175;
        }

        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            camnormal.m_YAxis.m_MaxSpeed = 2.5f;
            camnormal.m_XAxis.m_MaxSpeed = 250;
            camapuntada.m_YAxis.m_MaxSpeed = 2.5f;
            camapuntada.m_XAxis.m_MaxSpeed = 250;
        }

        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            camnormal.m_YAxis.m_MaxSpeed = 3.5f;
            camnormal.m_XAxis.m_MaxSpeed = 350;
            camapuntada.m_YAxis.m_MaxSpeed = 3.5f;
            camapuntada.m_XAxis.m_MaxSpeed = 350;
        }







    }

    private void LateUpdate()
    {
        ViewObstructed();
    }

    void ViewObstructed()
    {

        RaycastHit hit;


        if (Obstruction)
        {
            Obstruction.gameObject.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
        }

        if (Physics.Raycast(transform.position, Target.position - transform.position, out hit, 3f))
        {
            if (hit.collider.gameObject.tag != "Jugador"  && hit.collider.gameObject.tag != "Enemigo")
            {
                Obstruction = hit.transform;
                Obstruction.gameObject.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;

                if (Vector3.Distance(Obstruction.position, transform.position) >= 3f && Vector3.Distance(transform.position,Target.position) >= 1.5f)
                {
                    transform.Translate(Vector3.forward * zoomSpeed * Time.deltaTime);
                }


            }
            else
            {
                Obstruction.gameObject.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
                if (Vector3.Distance(transform.position, Target.position) < 3.5f)
                {
                    transform.Translate(Vector3.back * zoomSpeed * Time.deltaTime);
                }



            }
        }


    }
}
