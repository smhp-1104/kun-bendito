﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControladorDeTexto : MonoBehaviour
{
    public Text tutorialText;

    public Image ControlesMov;
    public Image ControlesSalto;
    public Image ControlesMelee;
    public Image ControlesDistancia;

    public float timer1;

    public bool act1;
    public bool act2;
    public bool act3;

    // Start is called before the first frame update
    void Start()
    {
        act1 = true;
        ControlesMov.enabled = false;
        ControlesSalto.enabled = false;
        ControlesMelee.enabled = false;
        ControlesDistancia.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        Aparicion();
        Objetivo();
        IndicacionesMovimiento();
        
    }

    void Aparicion()
    {
        if (act1 == true)
        {
            timer1 += Time.deltaTime;
            if (timer1 >= 5f)
            {
                tutorialText.text = "NOBLE SER, TIENES UNA MISIÓN";
                act1 = false;
                act2 = true;
            }
        }

    }

    void Objetivo()
    {
        if (act2 == true)
        {
            timer1 += Time.deltaTime;
            if (timer1 >= 8f)
            {
                tutorialText.text = "MOUNSTROS YOKAI ATACAN EL BOSQUE, DEBES DETENERLOS";
                act2 = false;
                act3 = true;
            }
        }
    }

    void IndicacionesMovimiento()
    {
        if(act3 == true)
        {
            timer1 += Time.deltaTime;
            {
                if(timer1 >= 12f)
                {
                    tutorialText.text = "MUEVETE CON LAS TECLAS WASD";
                    ControlesMov.enabled = true;
                    timer1 = 0;
                    act3 = false;

                }
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("TextoSalto"))
        {
            tutorialText.text = "SALTA CON LA BARRA ESPACIADORA";
            ControlesSalto.enabled = true;
            ControlesMov.enabled = false;
            Destroy(other.gameObject);
        }

        if(other.gameObject.CompareTag("TextoDash"))
        {
            tutorialText.text = "REALIZA UN DASH CON LA TECLA SHIFT";
            ControlesSalto.enabled = false;
            Destroy(other.gameObject);
        }

        if (other.gameObject.CompareTag("TextoAtaque"))
        {
            tutorialText.text = "ESE ES UNO DE ESOS MOUNSTROS, ACERCATE Y ENFRENTALO";      
            Destroy(other.gameObject);
        }

        if (other.gameObject.CompareTag("TextoAtaque2"))
        {
            tutorialText.text = "REALIZA UN ATAQUE MELEE CON CLICK IZQUIERDO";
            ControlesMelee.enabled = true;
            Destroy(other.gameObject);
        }

        if (other.gameObject.CompareTag("TextoAtaque3"))
        {
            tutorialText.text = "MANTEN TU DISTANCIA CON ESE," +
                " REALIZA UN ATAQUE A DISTANCIA CON CLICK DERECHO";
            ControlesDistancia.enabled = true;
            ControlesMelee.enabled = false;
            Destroy(other.gameObject);
        }

        if (other.gameObject.CompareTag("TextoIn"))
        {
            tutorialText.text = "EL TORI FINAL ESTA AHI AL FRENTE, NO FALTA MUCHO";
            ControlesMelee.enabled = false;
            ControlesDistancia.enabled = false;
            Destroy(other.gameObject);
        }

        if (other.gameObject.CompareTag("TextoIn2"))
        {
            tutorialText.text = "ESPERO HAYAMOS DERROTADO A TODOS O NO PODREMOS AVANZAR";
            ControlesDistancia.enabled = false;
            Destroy(other.gameObject);
        }

        if (other.gameObject.CompareTag("TextoIn3"))
        {
            tutorialText.text = "PARA PASAR A LA SIGUIENTE ZONA DEBES PASAR BAJO EL TORI";
            ControlesDistancia.enabled = false;
            Destroy(other.gameObject);
        }
    }

}
