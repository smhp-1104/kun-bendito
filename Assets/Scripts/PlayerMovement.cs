﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PlayerMovement : MonoBehaviour
{
    public float horizontalMove;
    public float verticalMove;
    public Vector3 playerInput;

    public CharacterController player;
    public Animator anim;

    public float playerSpeed;
    public Vector3 movePlayer;
    public float gravity = 9.8f;
    public float fallVelocity;
    public float jumpForce;
    public bool grounded;
    public float duraciondash;
    
    public bool isDashing;
    public float dashcd;
    public float dashingcd;
    private int dashAttempts;
    private float dashStartTime;

    [SerializeField] ParticleSystem forwardDashParticles;

    public Camera mainCamera;
    public Vector3 camForward;
    public Vector3 camRight;
    public Vector3 inputVector = Vector3.zero;
    public Transform playerPosition;
    public float enemySpeed;
    public float vision;
    public float turnSpeed = 10f;
    public bool lockon;
    public GameObject marcado;
    public bool marcando;



    void Start()
    {
        
        player = GetComponent<CharacterController>();
        lockon = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        mainCamera = Camera.main;

        Disparador_S disparador = GameObject.FindGameObjectWithTag("ArmaDisparo").GetComponent<Disparador_S>();
        disparador.disparando = false;
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        
    }

    void Update()
    {
        player.Move(movePlayer * Time.deltaTime);
        inputVector.x = Input.GetAxis("Horizontal");
        inputVector.z = Input.GetAxis("Vertical");

        playerInput = new Vector3(inputVector.x, 0, inputVector.z);
        playerInput = Vector3.ClampMagnitude(playerInput, 1);

        camDirection();

        movePlayer = playerInput.x * camRight + playerInput.z * camForward;

        Disparador_S disparador = GameObject.FindGameObjectWithTag("ArmaDisparo").GetComponent<Disparador_S>();
        FinalNivel1 final = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<FinalNivel1>();

        if (isDashing == false &&  disparador.disparando == false )
        {

            
            movePlayer = movePlayer * playerSpeed;
        }

        if (inputVector.x ==0 && inputVector.z == 0)
        {
            anim.SetBool("Run", false);
        }
        else
        {
            anim.SetBool("Run", true);
        }

        if (lockon == false)
        {
            if (disparador.disparando == false)
            {


                player.transform.LookAt(player.transform.position + movePlayer);
            }
            else
            {
                float yawCamera = mainCamera.transform.rotation.eulerAngles.y;
                transform.rotation = Quaternion.Euler(0, yawCamera, 0);
            }
            marcando = false;
        }

        SetGravity();


        //player.transform.LookAt(player.transform.position + movePlayer);



        if (disparador.disparando == true)
        {
            gravity = 0;
            fallVelocity = gravity * Time.deltaTime;
            Time.timeScale = 0.6f;
        }

        if (disparador.disparando == false)
        {
            gravity = 30;
            Time.timeScale = 1;
        }

        if (isDashing == false)
        {
            //SetJump();
        }
        SetJump();


        HandleDash();





        /*if (Input.GetKeyDown(KeyCode.F))
        {
            LockOn();
            
        }
        if (playerPosition == null)
        {
            lockon = false;
        }
        LockOn(); */



    }

    /*void LockOn()
    {
        
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemigo");
        if (enemies.Length > 0)
        {
            if (playerPosition == null)
            {
                playerPosition = enemies[0].transform;
            }
            float initialDistance = Vector3.Distance(playerPosition.position, transform.position);
            for (int i = 0; i < enemies.Length; i++)
            {
                float temporalDistance = Vector3.Distance(enemies[i].transform.position, transform.position);
                if (temporalDistance < initialDistance)
                {
                    
                    playerPosition = enemies[i].transform;
                    initialDistance = Vector3.Distance(playerPosition.position, transform.position);
                }

            }
        }
        float dist = Vector3.Distance(playerPosition.position, transform.position);
        if (dist < vision)
        {
            Vector3 dir = playerPosition.position - transform.position;
            Quaternion lookRotation = Quaternion.LookRotation(dir); // para que gire
            Vector3 rotation = Quaternion.Lerp(this.transform.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles; //smooth transitions
            this.transform.rotation = Quaternion.Euler(0f, rotation.y, 0f);
            lockon = true;

            if (marcando == false && lockon == true)
            {
                //Instantiate(marcado, playerPosition.transform.position, Quaternion.identity);
                marcando = true;
            }

        }
    }*/

        void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, vision);
    }

    void camDirection()
    {
        camForward = mainCamera.transform.forward;
        camRight = mainCamera.transform.right;

        camForward.y = 0;
        camRight.y = 0;

        camForward = camForward.normalized;
        camRight = camRight.normalized;
    }

    void HandleDash()
    {
        bool isTryingToDash = Input.GetKeyDown(KeyCode.LeftShift);
        Disparador_S disparador = GameObject.FindGameObjectWithTag("ArmaDisparo").GetComponent<Disparador_S>();

        dashingcd += Time.deltaTime;

        if (isTryingToDash && !isDashing && disparador.disparando == false && dashingcd >= dashcd)
        {
            if (dashAttempts <= 500)
            {
                OnStartDash();
                PlayDashParticles();
                dashingcd = 0;
            }
        }

        if (isDashing )
        {
            if (Time.time - dashStartTime <= duraciondash)
            {
                if (playerInput.Equals(Vector3.zero))
                {
                    player.Move(transform.forward * 30f * Time.deltaTime);
                }
                else
                {
                    movePlayer.z = movePlayer.z * (playerSpeed*3);
                    movePlayer.x = movePlayer.x * (playerSpeed * 3);
                }
                anim.SetBool("Dash", true);

            }
            else
            {
                OnEndDash();
            }
            
        }


    }

    void OnStartDash()
    {
        isDashing = true;
        dashStartTime = Time.time;
        //dashAttempts += 1;
    }

    void OnEndDash()
    {
        isDashing = false;
        dashStartTime = 0;
        anim.SetBool("Dash", false);
    }

    void PlayDashParticles()
    {


        
        forwardDashParticles.Play();
        //Vector3 inputVector player
    }



    void SetGravity()
    {
        if (player.isGrounded)
        {
            grounded = true;
            fallVelocity = -gravity * Time.deltaTime;
            movePlayer.y = fallVelocity;

        }

        else
        {
            grounded = false;
            fallVelocity -= gravity * Time.deltaTime;
            movePlayer.y = fallVelocity;
        }
    }

    void SetJump()
    {
        if (player.isGrounded && Input.GetButtonDown("Jump"))
        {
            fallVelocity = jumpForce;
            movePlayer.y = fallVelocity;
        }
    }
}
