﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KashaBullet : MonoBehaviour
{
    // Start is called before the first frame update

    public float vision;

    public Transform playerPosition;
    public GameObject target;
    public float turnSpeed = 10f;

    public Estadisticas_R playerStats;
    public int damage;
    public ParticleSystem deathParticles;

    public float enemySpeed;
    public bool uyquieto;
    public bool missed;


    void Start()
    {
        playerStats = GameObject.FindGameObjectWithTag("Jugador").GetComponent<Estadisticas_R>();
        missed = true;
        target = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        float dist = Vector3.Distance(target.transform.position, transform.position);
        if (dist < vision)
        {

            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, enemySpeed * Time.deltaTime);
            Vector3 dir = target.transform.position - transform.position;

        }

        if (missed == false)
        {
            target = GameObject.FindGameObjectWithTag("Kasha");
        }
        transform.Rotate(2, 1, 0 * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("ArmaPlayer"))
        {
            missed = false;
            enemySpeed = enemySpeed * 2;
        }

        if (other.gameObject.CompareTag("Bullet"))
        {
            Destroy(gameObject);
        }

        if (other.gameObject.CompareTag("Jugador") && missed == true)
        {
            //Instantiate(deathParticles, transform.position, Quaternion.identity);
            playerStats.life -= damage;
            Destroy(gameObject);
        }

    }


}
