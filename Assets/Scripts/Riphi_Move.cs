﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Riphi_Move : MonoBehaviour
{
    public float horizontalMove;
    public float verticalMove;
    public Vector3 playerInput;

    public CharacterController player;

    public float playerSpeed;
    public Vector3 movePlayer;
    public float gravity = 9.8f;
    public float fallVelocity;
    public float jumpForce;
    public bool grounded;
    public float duraciondash;

    public bool isDashing;
    private int dashAttempts;
    private float dashStartTime;

    [SerializeField] ParticleSystem forwardDashParticles;

    public Camera mainCamera;
    public Vector3 camForward;
    public Vector3 camRight;
    public Vector3 inputVector = Vector3.zero;



    void Start()
    {
        player = GetComponent<CharacterController>();

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        mainCamera = Camera.main;

        Disparador_S disparador = GameObject.FindGameObjectWithTag("ArmaDisparo").GetComponent<Disparador_S>();
    }

    // Update is called once per frame
    void Update()
    {
        inputVector.x = Input.GetAxis("Horizontal");
        inputVector.z = Input.GetAxis("Vertical");

        playerInput = new Vector3(inputVector.x, 0, inputVector.z);
        playerInput = Vector3.ClampMagnitude(playerInput, 1);

        camDirection();

        movePlayer = playerInput.x * camRight + playerInput.z * camForward;

        Disparador_S disparador = GameObject.FindGameObjectWithTag("ArmaDisparo").GetComponent<Disparador_S>();

        if (isDashing == false && disparador.disparando == false)
        {


            movePlayer = movePlayer * playerSpeed;
        }

       


        if (disparador.disparando == false)
        {


            player.transform.LookAt(player.transform.position + movePlayer);
        }
        else
        {
            float yawCamera = mainCamera.transform.rotation.eulerAngles.y;
            transform.rotation = Quaternion.Euler(0, yawCamera, 0);
        }


        //player.transform.LookAt(player.transform.position + movePlayer);

        SetGravity();

        if (disparador.disparando == true)
        {
            gravity = 0;
            fallVelocity = gravity * Time.deltaTime;
        }

        if (disparador.disparando == false)
        {
            gravity = 30;
        }

        if (isDashing == false)
        {
            //SetJump();
        }
        SetJump();


        HandleDash();


        player.Move(movePlayer * Time.deltaTime);


    }

    void camDirection()
    {
        camForward = mainCamera.transform.forward;
        camRight = mainCamera.transform.right;

        camForward.y = 0;
        camRight.y = 0;

        camForward = camForward.normalized;
        camRight = camRight.normalized;
    }

    void HandleDash()
    {
        bool isTryingToDash = Input.GetKeyDown(KeyCode.LeftShift);
        Disparador_S disparador = GameObject.FindGameObjectWithTag("ArmaDisparo").GetComponent<Disparador_S>();

        if (isTryingToDash && !isDashing && disparador.disparando == false)
        {
            if (dashAttempts <= 50)
            {
                OnStartDash();
                PlayDashParticles();
            }
        }

        if (isDashing)
        {
            if (Time.time - dashStartTime <= duraciondash)
            {
                if (playerInput.Equals(Vector3.zero))
                {
                    player.Move(transform.forward * 30f * Time.deltaTime);
                }
                else
                {
                    movePlayer.z = movePlayer.z * (playerSpeed * 3);
                    movePlayer.x = movePlayer.x * (playerSpeed * 3);
                }
            }
            else
            {
                OnEndDash();
            }
        }


    }

    void OnStartDash()
    {
        isDashing = true;
        dashStartTime = Time.time;
        //dashAttempts += 1;
    }

    void OnEndDash()
    {
        isDashing = false;
        dashStartTime = 0;
    }

    void PlayDashParticles()
    {



        forwardDashParticles.Play();
        //Vector3 inputVector player
    }



    void SetGravity()
    {
        if (player.isGrounded)
        {
            grounded = true;
            fallVelocity = -gravity * Time.deltaTime;
            movePlayer.y = fallVelocity;

        }

        else
        {
            grounded = false;
            fallVelocity -= gravity * Time.deltaTime;
            movePlayer.y = fallVelocity;
        }
    }

    void SetJump()
    {
        if (player.isGrounded && Input.GetButtonDown("Jump"))
        {
            fallVelocity = jumpForce;
            movePlayer.y = fallVelocity;
        }
    }
}
