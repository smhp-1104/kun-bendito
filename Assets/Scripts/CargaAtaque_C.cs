﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CargaAtaque_C : MonoBehaviour
{
    public float timer;
    public float maxTimer;
    public EnemigoOnibi_C onibi;
    // Start is called before the first frame update
    void Start()
    {
        onibi.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if(timer >= maxTimer)
        {
            onibi.enabled = true;
        }
    }
}
