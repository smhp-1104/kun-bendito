﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeInitiator : MonoBehaviour
{
    // Start is called before the first frame update

    public Animator fade;
    public float timer;

    private void Awake()
    {
        fade.SetBool("FadeIn", true);
    }

    void Start()
    {
        //fade.SetBool("FadeIn", false);
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (timer >= 1)
        {
            fade.SetBool("FadeIn", false);
        }
    }

    
}
