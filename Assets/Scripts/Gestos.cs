﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gestos : MonoBehaviour
{
    // Start is called before the first frame update
    public Estadisticas_R playerStats;
    public Image m_Image;
    public Image healthBar;
    public int health;
    public int startHealth;


    void Start()
    {
        playerStats = GameObject.FindGameObjectWithTag("Jugador").GetComponent<Estadisticas_R>();
        m_Image = GetComponent<Image>();
        startHealth = 100;
    }

    // Update is called once per frame
    void Update()
    {

        healthBar.fillAmount = playerStats.life / startHealth;


        if (playerStats.life <= 100 && playerStats.life > 75)
        {
            m_Image.sprite = playerStats.vida100;
        }

        if (playerStats.life <= 75 && playerStats.life > 50)
        {
            m_Image.sprite = playerStats.vida75;
        }

        if (playerStats.life <= 50 && playerStats.life > 25)
        {
            m_Image.sprite = playerStats.vida50;
        }

        if (playerStats.life <= 25 && playerStats.life > 0)
        {
            m_Image.sprite = playerStats.vida25;
        }
    }
}
