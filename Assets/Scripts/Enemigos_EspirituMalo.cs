﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigos_EspirituMalo : MonoBehaviour
{
    // Start is called before the first frame update

    public float vision;

    public Transform playerPosition;

    public float turnSpeed = 10f;

    public float enemySpeed;
    public bool uyquieto;

    //Ataque
    public Estadisticas_R playerStats;
    public float atackTimer = 1.5f;
    public bool switchatack;

    void Start()
    {
        playerStats = GameObject.FindGameObjectWithTag("Jugador").GetComponent<Estadisticas_R>();
    }

    // Update is called once per frame
    void Update()
    {
        float dist = Vector3.Distance(playerPosition.position, transform.position);
        if (dist < vision)
        {

            transform.position = Vector3.MoveTowards(transform.position, playerPosition.transform.position, enemySpeed * Time.deltaTime);
            Vector3 dir = playerPosition.position - transform.position;
            Quaternion lookRotation = Quaternion.LookRotation(dir); // para que gire
            Vector3 rotation = Quaternion.Lerp(this.transform.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles; //smooth transitions
            this.transform.rotation = Quaternion.Euler(0f, rotation.y, 0f);
        }

        if (dist <= 1)
        {
            uyquieto = true;
        }
        else
        {
            uyquieto = false;
        }

        if (uyquieto == true)
        {
            enemySpeed = 0;
        }
        else
        {
            enemySpeed = 10;
        }

        Atack();
    }

    void Atack()
    {
        if (switchatack == true)
        {
            atackTimer -= Time.deltaTime;
            if (atackTimer <= 0)
            {
                playerStats.life -= 1;
                atackTimer = 1.5f;
            }

        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, vision);
    }



    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("ArmaPlayer"))
        {
            Destroy(gameObject);
        }

        if (other.gameObject.CompareTag("Jugador"))
        {
            switchatack = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Jugador"))
        {
            switchatack = false;
        }
    }
}
