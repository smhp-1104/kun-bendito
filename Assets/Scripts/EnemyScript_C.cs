﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript_C : MonoBehaviour
{
    // Start is called before the first frame update

    public float vision;

    public Transform playerPosition;
    public int cooldown;
    public Animator anim;

    private float turnSpeed = 10f;
    private float enemySpeed;
    public float speed;
    public bool uyquieto;

    //Ataque
    public Estadisticas_R playerStats;
    private float atackTimer = 3;
    public bool switchatack;
    public Rigidbody rb;

    void Start()
    {
        playerStats = GameObject.FindGameObjectWithTag("Jugador").GetComponent<Estadisticas_R>();
        playerPosition = GameObject.FindGameObjectWithTag("Jugador").GetComponent<Transform>();
        
    }

    // Update is called once per frame
    void Update()
    {

        float dist = Vector3.Distance(playerPosition.position, transform.position);
        if (dist < vision)
        {

            transform.position = Vector3.MoveTowards(transform.position, playerPosition.transform.position, enemySpeed * Time.deltaTime);
            Vector3 dir = playerPosition.position - transform.position;
            Quaternion lookRotation = Quaternion.LookRotation(dir); // para que gire
            Vector3 rotation = Quaternion.Lerp(this.transform.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles; //smooth transitions
            this.transform.rotation = Quaternion.Euler(0f, rotation.y, 0f);

            anim.SetBool("Run", true);
        }
        else
        {
            anim.SetBool("Run", false);
        }

        if (dist <= 1f)
        {
            uyquieto = true;
            switchatack = true;
            
        }
        else
        {
            uyquieto = false;
             
        }

        if (uyquieto == true)
        {
            enemySpeed = 0;
            

        }
        else
        {
            enemySpeed = speed;
            switchatack = false;
        }


        atackTimer += Time.deltaTime;

        Atack();
    }

    void Atack()
    {
        if(switchatack == true && atackTimer >cooldown)
        {
            anim.SetBool("Attack", true);
            atackTimer = 0;

        }
        else
        {
            anim.SetBool("Attack", false);
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, vision);
    }



    void OnTriggerEnter(Collider other)
    {


    }




}
