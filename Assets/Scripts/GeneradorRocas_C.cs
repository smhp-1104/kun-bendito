﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneradorRocas_C : MonoBehaviour
{
    public GameObject Roca;
    public float timer;

    // Start is called before the first frame update
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {
        Generator();
    }

    void Generator()
    {
        timer += Time.deltaTime;
        if (timer >= 4f)
        {
            Instantiate(Roca);
            Roca.transform.position = transform.position;
            timer = 0;
            return;
        }
    }
}
