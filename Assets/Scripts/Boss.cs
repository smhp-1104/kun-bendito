﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss : MonoBehaviour
{
    public GameObject Onibi;
    public float timer;

    // Start is called before the first frame update
    void Start()
    {
        Generator();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Generator()
    {

            Instantiate(Onibi);
            Onibi.transform.position = transform.position;

    }
}
