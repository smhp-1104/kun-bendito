﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanonLook : MonoBehaviour
{
    // Start is called before the first frame update

    Camera mainCamera;
    public float turnSpeed = 15;

    void Start()
    {
        mainCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        float yawCamera = mainCamera.transform.rotation.eulerAngles.y;
        float yawCamera2 = mainCamera.transform.rotation.eulerAngles.x;
        transform.rotation = Quaternion.Euler(0, yawCamera, 0);
    }
}
