﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PlayerAttack_S : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject ArmaIddle;
    public GameObject ArmaMelee;
    public GameObject player;
    public GameObject puntoarmaPlayer;
    public GameObject armaPlayer;

    public Animator anim;

    public float timer;
    public float attacktimer;
    public bool attacking;

    public CinemachineFreeLook camNormal;
    public CinemachineFreeLook camApuntada;
    public CinemachineVirtualCamera camInicial;
    public AudioSource meleeAttack;

    public Collider armasPlayer;
    
    void Start()
    {
        attacktimer = 0.5f;

        camInicial.Priority = 200;
        player.GetComponent<PlayerMovement>().enabled = false;
        player.GetComponent<CinemachineModifier>().enabled = true;

        puntoarmaPlayer.GetComponent<TrailRenderer>().enabled = false;
        armaPlayer.GetComponent<CapsuleCollider>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        attacktimer += Time.deltaTime;

        if (timer >= 2)
        {
            camNormal.Priority = 100;
            camInicial.Priority = 10;

        }
        if (timer >= 4.5)
        {
            player.GetComponent<PlayerMovement>().enabled = true;
            camNormal.m_Transitions.m_InheritPosition = true;

        }

        if (this.anim.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
        {
            puntoarmaPlayer.GetComponent<TrailRenderer>().enabled = true;
            armaPlayer.GetComponent<CapsuleCollider>().enabled = true;
        }
        else
        {
            puntoarmaPlayer.GetComponent<TrailRenderer>().enabled = false;
            armaPlayer.GetComponent<CapsuleCollider>().enabled = false;
        }

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {

            anim.SetBool("Attack", true);
            LaunchAttack(armasPlayer);
            



        }

        if (Input.GetKeyDown(KeyCode.Mouse0) && attacktimer >= 0.7)
        {
            meleeAttack.Play();
            attacktimer = 0;
        }

        if (attacktimer >= 0.5)
        {
            anim.SetBool("Attack", false);
        }

        if (Input.GetKeyUp(KeyCode.Mouse0))
        {

            anim.SetBool("Attack", false);

        }

        Disparador_S disparador = GameObject.FindGameObjectWithTag("ArmaDisparo").GetComponent<Disparador_S>();

        if (disparador.disparando == true)
        {

            anim.SetBool("Shooting", false);
            anim.SetBool("Shoot", true);

        }
        else
        {
            anim.SetBool("Shoot", false);
            anim.SetBool("Shooting", true);

        }
        if (this.anim.GetCurrentAnimatorStateInfo(0).IsName("Disparo2"))
        {
            anim.SetBool("Shooting", false);
        }



    }

    void LaunchAttack (Collider col) 
    {
        Collider[] cols = Physics.OverlapBox(col.bounds.center, col.bounds.extents, col.transform.rotation, LayerMask.GetMask("HitBox"));
        foreach (Collider c in cols)
            Debug.Log(c.name);
    }
}
