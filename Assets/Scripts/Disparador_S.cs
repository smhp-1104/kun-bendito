﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.UI;

public class Disparador_S : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject Bullet;
    public float propulsionForce;
    public AudioSource mAudioSrc;
    public bool active;
    public Canvas CanvasApuntado;
    
    public float shoottimer;
    public float maxshoottimer;
    public Image FillImage;

    public CinemachineFreeLook camNormal;
    public CinemachineFreeLook camApuntada;
    public bool disparando;

    public Transform myTransform;
    public Transform target;
    public CinemachineBrain cinemachineBrain;

    void Start()
    {
        maxshoottimer = 0.8f;
        myTransform = transform;
        CanvasApuntado.enabled = false;
        disparando = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            cinemachineBrain.m_DefaultBlend.m_Time = 0.5f;
            camNormal.Priority = 10;
            camApuntada.Priority = 1000;
            disparando = true;
            active = true;
            CanvasApuntado.enabled = active;

        }

        if (disparando == true)
        {
            shoottimer += Time.deltaTime;
        }

        //if (Input.GetKeyUp(KeyCode.Mouse1))

        if (shoottimer >= maxshoottimer)
        {
            active = false;
            CanvasApuntado.enabled = active;
            mAudioSrc.Play();
            SpawnBullet();
            camApuntada.Priority = 10;
            camNormal.Priority = 1000;
            cinemachineBrain.m_DefaultBlend.m_Time = 2;
            disparando = false;
            shoottimer = 0;
        }

        FillImage.fillAmount = shoottimer / maxshoottimer;


    }

    void SpawnBullet()
    {
        GameObject Luz = (GameObject)Instantiate(Bullet, myTransform.transform.TransformPoint(0, 0, 1f), myTransform.rotation);
        Luz.GetComponent<Rigidbody>().AddForce(target.forward * propulsionForce, ForceMode.Impulse);
    }

}
