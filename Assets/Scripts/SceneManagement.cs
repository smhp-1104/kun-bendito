﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagement : MonoBehaviour
{
    // Start is called before the first frame update
    
    public Animator fade;
    public float fadetimer;
    public string sceneName;
    public GameObject boton;
    public GameObject boton2;
    public GameObject boton3;
    public bool creditos;

    void Start()
    {
        creditos = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (this.fade.GetCurrentAnimatorStateInfo(0).IsName("FadeOut"))
        {
            fadetimer += Time.deltaTime;
        }
        ChangeScene();
    }

    public void ChangeScene()
    {  

        if (fadetimer >= 1 && creditos == false)
        {
            SceneManager.LoadScene(sceneName);

        }
        if (fadetimer >= 1 && creditos == true)
        {
            SceneManager.LoadScene("Creditos");

        }

    }

    public void ChangeSceneCredits()
    {

        creditos = true;
        Destroy(boton);
        Destroy(boton2);
        Destroy(boton3);
        fade.SetBool("FadeOut", true);

    }

    public void SetChange()
    {
        Destroy(boton);
        Destroy(boton2);
        Destroy(boton3);
        fade.SetBool("FadeOut", true);
    }

    public void Salir()
    {
        Debug.Log("Has Salido");
        Application.Quit();
    }
}
