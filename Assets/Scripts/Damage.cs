﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage : MonoBehaviour
{
    // Start is called before the first frame update
    public int damage;
    public Estadisticas_R playerStats;

    void Start()
    {
        playerStats = GameObject.FindGameObjectWithTag("Jugador").GetComponent<Estadisticas_R>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Jugador"))
        {
            playerStats.life -= damage;
        }
    }
}
