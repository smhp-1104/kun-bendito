﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Roca_C : MonoBehaviour
{
    public float timer;
    public float maxTimer;
    public bool destruccion;

    public Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();

    }

    // Update is called once per frame
    void Update()
    {
        if(destruccion == true)
        {
            timer += Time.deltaTime;
            if(timer >= maxTimer)
            {
                Destroy(gameObject);
                destruccion = false;
            }
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Suelo"))
        {
            destruccion = true;
        }
    }
}
