﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FinalNivel1 : MonoBehaviour
{
    public Estadisticas_R player;

    public Text textito;

    public Animator fade;

    public float fadetimer;

    public bool fading;

    public GameObject playerobj;

    public int nivel;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Jugador").GetComponent<Estadisticas_R>();
        fading = false;

    }

    // Update is called once per frame
    void Update()
    {
        if (fadetimer >= 1 && nivel == 1)
        {
            SceneManager.LoadScene("Nivel1");

        }

        if (fadetimer >= 1 && nivel == 2)
        {
            SceneManager.LoadScene("Nivel2");

        }

        if (this.fade.GetCurrentAnimatorStateInfo(0).IsName("FadeOut"))
        {
            fadetimer += Time.deltaTime;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
       

        if (other.gameObject.CompareTag("Jugador"))
        {


            if (nivel == 1 && player.enemigos == 3)
            {
                playerobj.GetComponent<PlayerAttack_S>().enabled = false;
                playerobj.GetComponent<PlayerMovement>().enabled = false;
                playerobj.GetComponent<Animator>().enabled = false;
                fade.SetBool("FadeOut", true);
                fading = true;
            }

            if (nivel == 2 && player.enemigos == 15)
            {
                playerobj.GetComponent<PlayerAttack_S>().enabled = false;
                playerobj.GetComponent<PlayerMovement>().enabled = false;
                playerobj.GetComponent<Animator>().enabled = false;
                fade.SetBool("FadeOut", true);
                fading = true;
            }
        }

    }
}
