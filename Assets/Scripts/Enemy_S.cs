﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_S : MonoBehaviour
{
    // Start is called before the first frame update

    public float vision;

    public Transform playerPosition;
    public float turnSpeed = 10f;

    public ParticleSystem deathParticles;

    public float enemySpeed;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float dist = Vector3.Distance(playerPosition.position, transform.position);
        if (dist < vision)
        {

            transform.position = Vector3.MoveTowards(transform.position, playerPosition.transform.position, enemySpeed * Time.deltaTime);
            Vector3 dir = playerPosition.position - transform.position;
            Quaternion lookRotation = Quaternion.LookRotation(dir); // para que gire
            Vector3 rotation = Quaternion.Lerp(this.transform.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles; //smooth transitions
            this.transform.rotation = Quaternion.Euler(0f, rotation.y, 0f);
        }

        if (dist <= 1.5f)
        {
            enemySpeed = 0;
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, vision);
    }



    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("ArmaPlayer"))
        {
            Instantiate(deathParticles, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
