﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class KashaScript : MonoBehaviour
{
    // Start is called before the first frame update

    public Animator anim;
    public float initialtimer;
    public float attackcdtimer;
    public float intervaloAtaque;
    public GameObject BulletSpawnPoint;
    public GameObject Bullet;
    public HealthBar healthbar;
    public int dmgfromPlayer;
    public int dmgfromBullet;
    public float timer;
    public Animator fade;
    public float fadetimer;

    public SkinnedMeshRenderer nupMesh;
    public Material matNormal;
    public Material matHitted;
    public bool inzone;

    void Start()
    {
        healthbar.damage = dmgfromPlayer;
        inzone = false;
        
        //Quitar al fixear Kasha
    }

    // Update is called once per frame
    void Update()
    {
        initialtimer += Time.deltaTime;
        attackcdtimer += Time.deltaTime;
        //fadetimer += Time.deltaTime;
        timer += Time.deltaTime;

        if (healthbar.health <= 0)
        {
            fade.SetBool("FadeOut", true);

        }

        if (this.fade.GetCurrentAnimatorStateInfo(0).IsName("FadeOut"))
        {
            fadetimer += Time.deltaTime;
        }

        if (fadetimer >= 1)
        {
            SceneManager.LoadScene("Victoria");
            
        }

        if (initialtimer >= 1)  // A cambiar 0 por 1 al fixear Kasha
        {
            anim.SetBool("Start", false);


        }

        if (initialtimer < 1)  // A cambiar 0 por 1 al fixear Kasha
        {

            anim.SetBool("Start", true);

        }


        if (attackcdtimer > 0 && attackcdtimer < 1)
        {
            anim.SetBool("Attack", false);
        }

        if (attackcdtimer >= intervaloAtaque -1 && inzone == true)
        {
            anim.SetBool("Attack", true);
            
        }
        if (attackcdtimer >= intervaloAtaque && inzone == true)
        {
            

            GameObject temporalbullet = Instantiate(Bullet);
            temporalbullet.transform.position = BulletSpawnPoint.transform.position;


            attackcdtimer = 0;
        }

        if (timer >= 0.3)
        {
            nupMesh.material = matNormal;
        }
    }
    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Enemigo"))
        {
            if (healthbar && timer >= 0.5)
            {
                timer = 0;
                nupMesh.material = matHitted;
                healthbar.damage = dmgfromPlayer;
                healthbar.OnTakeDamage();
                

            }
            Destroy(collision.gameObject);
        }

        if (collision.gameObject.CompareTag("Bullet"))
        {
            if (healthbar && timer >= 0.5)
            {
                timer = 0;
                healthbar.damage = dmgfromBullet;
                healthbar.OnTakeDamage();

            }
        }


    }
}
