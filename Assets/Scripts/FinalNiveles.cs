﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FinalNiveles : MonoBehaviour
{
    public string escena;

    public Estadisticas_R player;

    public Text textito;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Jugador").GetComponent<Estadisticas_R>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Jugador") && player.enemigos == 3)
        {
            SceneManager.LoadScene("Nivel1");
        }
        else
        {
            textito.text = "AUN TE QUEDAN ENEMIGOS POR DERROTAR, ¡BUSCALOS!";
        }
    }
}
