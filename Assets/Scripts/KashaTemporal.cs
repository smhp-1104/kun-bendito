﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KashaTemporal : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject Kasha;
    public float timer;
    public bool kashaenabled;
    public Animator anim;
    public float initialtimer;
    void Start()
    {
        anim.SetBool("Start", true);
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        initialtimer += Time.deltaTime;

        if (timer >= 2.8f && kashaenabled == false)
        {
            Kasha.SetActive(true);
            kashaenabled = true;
            Destroy(gameObject);
        }

    }
}
