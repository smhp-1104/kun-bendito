﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoOnibi_C : MonoBehaviour
{// Start is called before the first frame update

    public float vision;

    public Estadisticas_R player;

    public Transform playerPosition;
    public Estadisticas_R playerStats;
    public float turnSpeed = 10f;
    public int damage;

    public ParticleSystem deathParticles;

    public float enemySpeed;
    public bool uyquieto;

    void Start()
    {
        playerStats = GameObject.FindGameObjectWithTag("Jugador").GetComponent<Estadisticas_R>();
        playerPosition = GameObject.FindGameObjectWithTag("Jugador").GetComponent<Transform>();

        player = GameObject.FindGameObjectWithTag("Jugador").GetComponent<Estadisticas_R>();


    }

    // Update is called once per frame
    void Update()
    {
        float dist = Vector3.Distance(playerPosition.position, transform.position);
        if (dist < vision)
        {

            transform.position = Vector3.MoveTowards(transform.position, playerPosition.transform.position, enemySpeed * Time.deltaTime);
            Vector3 dir = playerPosition.position - transform.position;
            Quaternion lookRotation = Quaternion.LookRotation(dir); // para que gire
            Vector3 rotation = Quaternion.Lerp(this.transform.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles; //smooth transitions
            this.transform.rotation = Quaternion.Euler(0f, rotation.y, 0f);
        }

    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, vision);
    }

    void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Jugador"))
        {
            player.enemigos += 1;
            Instantiate(deathParticles, transform.position, Quaternion.identity);
            playerStats.life -= damage;
            Destroy(gameObject);

        }
    }



    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Bullet"))
        {
            player.enemigos += 1;
            Instantiate(deathParticles, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
