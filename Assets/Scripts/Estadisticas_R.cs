﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Estadisticas_R : MonoBehaviour
{
    public float life;
    public Image BarradeVida;

    public int enemigos;
    public int maxenemigos;
    public Text numenemigos;
    public Text noenemigos;

    public Riphi_Move player;
    public GameObject playerObject;

    public SpriteRenderer caritacongesto;
    public Sprite vida100;
    public Sprite vida75;
    public Sprite vida50;
    public Sprite vida25;
    public string scene;

    // Start is called before the first frame update
    void Start()
    {
        noenemigos.text = "";
        
    }

    // Update is called once per frame
    void Update()
    {
        if(life <= 0)
        {
            SceneManager.LoadScene(scene);
        }


        life = Mathf.Clamp(life, 0, 100);

        BarradeVida.fillAmount = life / 100;

        if (enemigos < 6)
        {
            numenemigos.text = "" + (maxenemigos - enemigos);
        }
        if (enemigos >= 6 && enemigos != maxenemigos)
        {
            numenemigos.text = "0" + (maxenemigos - enemigos);
        }
        if (enemigos == maxenemigos)
        {
            numenemigos.text = "";
            noenemigos.text = "Has purgado a todos";
        }


    }

    private void OnTriggerEnter(Collider other)
    {

    }
}
