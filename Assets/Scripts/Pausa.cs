﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pausa : MonoBehaviour
{
    public bool active;
    public Canvas MenuPausa;
    

    void Start()
    {
        MenuPausa = GetComponent<Canvas>();
        MenuPausa.enabled = false;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            Debug.Log("Esta Pausado");
            active = !active;
            MenuPausa.enabled = active;
            Time.timeScale = (active) ? 0 : 1f;
        }

        if (active == true)
        {
            if (Input.GetKeyDown(KeyCode.M))
            {
                active = false;
                MenuPausa.enabled = false;
                Time.timeScale = 1f;
                SceneManager.LoadScene("MenuPrincipal");
            }
        }

    }
}
