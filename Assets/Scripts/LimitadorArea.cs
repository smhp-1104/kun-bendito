﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimitadorArea : MonoBehaviour
{
    // Start is called before the first frame update

    public BoxCollider triggerbox;
    public BoxCollider box;

    void Start()
    {
        box.enabled = false;
        triggerbox.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Jugador"))
        {
            triggerbox.enabled = false;
            box.enabled = true;            
            KashaScript kasha = GameObject.FindGameObjectWithTag("KashaBoss").GetComponent<KashaScript>();
            kasha.inzone = true;
            kasha.attackcdtimer = 4;

        }
    }
}
